---
title: Home
sections:
  - section_id: hero
    type: section_hero
    title: RGY SERVICIOS CONTABLES.
    image: images/5.jpg
    content: >-
      De manera legal, responsable y puntual.
    actions:
      - label: Contáctanos
        url: https://www.rgyservicioscontables-contact.ml/
        style: primary
  - section_id: features
    type: section_grid
    col_number: three
    grid_items:
      - title: ASESORÍAS ADMINISTRATIVAS
        content: >-
          * Asesoría en creación y constitución de sociedades. 

          * Asesoría en temas de nómina y talento humano.
     
          * Liquidación de contratos y prestaciones sociales. 

          * Asesoría en temas documentales. 
        actions:
          - label: Solicitar Asesoría
            url: https://www.rgyservicioscontables-contact.ml/
            style: link
      - title: TEMAS CONTABLES
        content: >-
          * Contabilidades para personas NATURALES Y JURÍDICAS.

          * Contabilidades para empresas del sector REAL Y SOLIDARIO. 

          * Elaboración de presupuestos. 

          * Elaboración de estados financieros. 

          * Liquidación de impuestos nacionales, distritales y municipales. 

          * Asesoría en trámites ante las entidades de impuestos nacionales, distritales y municipales. 
        actions:
          - label: Solicitar Servicio
            url: https://www.rgyservicioscontables-contact.ml/
            style: link
      - title: PRESENTACIÓN DE REPORTES E INFORMES
        content: >-
          * Asesoría en la presentación de informes y reportes a entidades de vigilancia y control (DANE, SUPERINTENDENCIAS). 

          * Asesoría y presentación de información exógena a nivel nacional distrital y municipal.  
        actions:
          - label: Ayuda en el trámite
            url: https://www.rgyservicioscontables-contact.ml/
            style: link
  - section_id: text-img
    type: section_content
    image: images/imagen-ryg.svg
    image_position: left
    title: Servicios de AUDITORÍA Y REVISORÍA FISCAL 
    content: >-
      Revisoría fiscal y Auditoría Financiera realizada de manera integral, lo cual nos permite emitir una opinión y 
      recomendaciones pertinentes a nivel administrativo, contable y financiero.
    actions:
      - label: Contactar
        url: https://www.rgyservicioscontables-contact.ml/
        style: primary
  - section_id: cta
    type: section_cta
    title: Si necesitas ayuda no dudes en contactarnos
    subtitle: Puedes hacerlo por el formulario de solicitud de la página.
    actions:
      - label: Solicitar
        url: https://www.rgyservicioscontables-contact.ml/
        style: primary
seo:
  title: RGY Servicios Contables
  description: Servicios Contables
  extra:
    - name: 'og:type'
      value: website
      keyName: property
    - name: 'og:title'
      value: Servicios Contables
      keyName: property
    - name: 'og:description'
      value: Servicios Contables
      keyName: property
    - name: 'og:image'
      value: images/4.jpg
      keyName: property
      relativeUrl: true
    - name: 'twitter:card'
      value: summary_large_image
    - name: 'twitter:title'
      value: Servicios Contables
    - name: 'twitter:description'
      value: Servicios Contables
    - name: 'twitter:image'
      value: images/4.jpg
      relativeUrl: true
layout: advanced
---
